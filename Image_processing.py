#!/usr/bin/env python
# coding: utf-8

# In[701]:


#import useful libraries
#plot graphics in the notebook
import numpy as np
get_ipython().run_line_magic('matplotlib', 'inline')
#Matplotlib's imperative-style plotting interface, pyplot
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg
from PIL import Image

#Example for night sky
import cv2
from skimage import data
from skimage.feature import *
from math import sqrt
from skimage.color import rgb2gray
import glob
from skimage.io import imread


# In[702]:


#import image
img = image.imread("D:\MSc Computer Science\Advanced Topics in Computer Science\Digital Image Processing\My Library\happy_bman.png")


# In[703]:


#Shape of the image
print(img.shape)
#Data type: 
print(type(img))
#Printing image
#print(img)


# In[704]:


#plotting image in the notebook
imgplot =plt.imshow(img)


# In[705]:


#enhancing contrast and visualizing your data using pseudo colors
lum_img = img[:,:,0]
plt.imshow(lum_img)
#plotting the values each color represents
plt.colorbar()


# In[706]:


#Applying default color map, viridis (default) or any other
plt.imshow(lum_img, cmap="hot")


# In[707]:


#change colormaps on existing plot objects using the set_cmap()
imgplot = plt.imshow(lum_img)
imgplot.set_cmap('nipy_spectral')


# In[708]:


#If we are to change image and compromise on things like color
#find interesting regions using histogram
plt.hist(lum_img.ravel(), bins=256, range=(0.0, 1.0), fc='k', ec='k')


# In[709]:


#clipping the regions to get get extra contrast
imgplot = plt.imshow(lum_img, clim = (0.0, 0.7))


# In[710]:


#Another opion of specifying clim using the returned object
fig = plt.figure()
ax = fig.add_subplot(1, 2, 1)
imgplot = plt.imshow(lum_img)
ax.set_title('Before')
plt.colorbar(ticks=[0.1, 0.3, 0.5, 0.7], orientation='horizontal')
ax = fig.add_subplot(1, 2, 2)
imgplot = plt.imshow(lum_img)
imgplot.set_clim(0.0, 0.7)
ax.set_title('After')
plt.colorbar(ticks=[0.1, 0.3, 0.5, 0.7], orientation='horizontal')


# In[711]:


#Shrinking image while maintaining the important pixels
img = Image.open('D:\MSc Computer Science\Advanced Topics in Computer Science\Digital Image Processing\My Library\happy_bman.png')
img.thumbnail((64, 64), Image.ANTIALIAS)  # resizes image in-place
imgplot = plt.imshow(img)


# In[712]:


#bilinear interpolation
imgplot = plt.imshow(img, interpolation = "nearest")


# In[713]:


#Bicubic interpolation 
imgplot = plt.imshow(img, interpolation="bicubic")


# In[714]:


#Another example of counting stars in the night sky using machine learning
#import the image of the sky
#sky_img = image.imread("D:\MSc Computer Science\Advanced Topics in Computer Science\Digital Image Processing\My Library\skynight.png")
sky = cv2.imread("D:\MSc Computer Science\Advanced Topics in Computer Science\Digital Image Processing\My Library\skynight.png")
sky_img = glob.glob(r"D:\MSc Computer Science\Advanced Topics in Computer Science\Digital Image Processing\My Library\skynight.png")[0]


# In[715]:


#Displaying the night sky image
imgplot =plt.imshow(sky)
#print(sky_img)
print("Image properties")
print("- Number ofpixels: " + str(sky.size))
print("- Shape/Dimensions: " + str(sky.shape))


# In[716]:


im = imread(sky_img, as_gray=True)
plt.imshow(im,cmap=cm.gray)
plt.show()


# In[717]:


#Fing the data type of the array for the image.Determine the datatype
#<U108 doesnt change the image into the format of the float datatype
np.array("D:\MSc Computer Science\Advanced Topics in Computer Science\Digital Image Processing\My Library\skynight.png")


# In[718]:


#skyplot =plt.imshow(sky_img)
blobs_log = blob_log(im, max_sigma=30, num_sigma=10, threshold=.1)
numrows = len(blobs_log)
print("Number of stars counted are : ", numrows)


# In[719]:


#Validation to see whether all stars were captured
fig, ax = plt.subplots(1, 1)
plt.imshow(im, cmap=cm.gray)
for blob in blobs_log:
    y, x, r = blob
    c = plt.Circle((x, y), r+5, color='lime', linewidth=2, fill=False)
    ax.add_patch(c)


# In[720]:


#Import libraries for facial recognition
from cv2 import *
import cv2

# print version number
print(cv2.__version__)


# In[721]:


# load the photograph with faces to analyse
faces_img = cv2.imread(r'D:\MSc Computer Science\Advanced Topics in Computer Science\Digital Image Processing\My Library\faces.png')


# In[722]:


face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')


# In[723]:


img_face = cv2.cvtColor(faces_img, cv2.COLOR_BGR2GRAY)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




